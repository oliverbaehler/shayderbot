# ShayderBot

One day i am going to rule the world.
A just for fun Twitch Bot for (https://www.twitch.tv/noiki).

## Commands

Following Commands are allowed:

| Command | Description | Master Privileges | Owner Privileges |
| :------ | :---------- | :---------------- | :--------------- |
|<code>!shaydehelp</code>| Bot will reference this page and it's version | | |
|<code>!shaydeoff</code>|Bot won't execute functions| x | |
|<code>!shaydeon</code>|Bot will listen to given channels and execute functions| x | |
|<code>!masteradd <@user></code>|Add a master with advanced privileges | x | |
|<code>!masterrm <@user></code>|Remove a master with advanced privileges | | x |
|<code>!masters</code>|Show current masters| | |
|<code>!addmsg <msg></code>|Add a Shayde Message to database| x | |
|<code>!shotset <int></code>|Define how many shots were taken | x | |
|<code>!shotup</code>|Increment shot counter (Master)| x | |
|<code>!shotup</code>|Decrement shot counter (Master)| x | |
|<code>!shots</code>|Show Shot count|  | |
|<code>!shotreset</code>|Reset Counter to 0 | x | |
