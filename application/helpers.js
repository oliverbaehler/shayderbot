








//
// Unpermitted Reply (When not Master/Owner)
//
function unpermittedReply(user) {
	var userRef = "@" + user; // User Reference
	var responses = [
    userRef + " you are not my master, ass",
		userRef + " i don't like it",
		userRef + " i won't do anything without chocolate",
		userRef + " no",
		userRef + " i don't know",
		userRef + " what do you want",
		userRef + " don't tell me what to do"
	];
	return responses[randomNumber(responses.length)]; // Return Response
}

//
// Shot Reply
//
function shotReply(shotcount) {
	var responses = [
    "Currently flexing on " + shotcount + " shot(s)",
		"I am feeling it (" + shotcount + " shots)",
		shotcount + " shot(s) !yeet"
	];
	return responses[randomNumber(responses.length)]; // Return Response
}

//
// Random Number
//
function randomNumber(rangeMax) {
	return Math.floor(Math.random() * rangeMax);
}

//
// Get Just the User for function
//
function userParameter(message, removeOption) {
	var userReferenced = message.replace(removeOption,'').toLowerCase();
	return [ userReferenced, userReferenced.replace('@', '')];
}

//
// Recreate User Reference
//
function recreateReference(msg, user) {
  return msg.replace('{{user}}', user);
}

//
// Remove User Reference
//
function anonymReference(msg, targets) {
	let targetNames = arraySplitter(targets, "|"); // Get Targets in splitted string
	return msg.replace(/@(?! + targetNames + )[a-zA-Z0-9_\-\!\.\?]+/g, "{{user}}"); // Replace References (but not target references)
}

//
// Check if is Master
//
function isMaster(user, database) {

  // Check if User is Owner
	if (user == configuration.owner) {
		return true; // Return True on Owner
	} else {

		// Check Master Database
		database.run('SELECT master FROM masters WHERE master = ' + user,  function(err, result) {
			if (err) {
				console.log(err);
			} else {
        console.log(result);

				// Check Database Result
				if (result != undefined) {
					return true;
				} else {
					return false;
				}
			}
	  });
  }
}

//
// Add Message to Database
//
function addDatabaseMessage(message, channel, messageOrigin, database) {
	var anonymMsg = anonymReference(message, configuration.targets);
  // Insert Data into Database
  database.run('INSERT OR IGNORE INTO messages VALUES (NULL, ?, ?, ?)', [anonymMsg, channel, messageOrigin], function(err, result) {
		  if (err) {
				console.log(err);
			} else {
				console.log("New Message added - " + anonymMsg + " for Channel - " + channel + ", Message Origin - " + messageOrigin); // Logging
			}
	  });
}

//
// Split Array to String
//
function arraySplitter(dataArray, splitter) {

	// Create Reference Regex
	var names = "";
	for (var i = 0; i <= dataArray.length - 1; i++) {
    names += dataArray[i];
		if (i != dataArray.length - 1) names += splitter
	}

	// Return splitted String
	return names;
}





// Export Module
module.exports = {
    unpermittedReply: unpermittedReply,
    shotReply: shotReply,
    randomNumber: randomNumber,
    userParameter: userParameter,
    isMaster: isMaster,
    addDatabaseMessage: addDatabaseMessage,
    recreateReference: recreateReference,
    anonymReference: anonymReference,
    arraySplitter: arraySplitter
}
