
const tmi = require('tmi.js'),
  sqlite3 = require('sqlite3').verbose(),
  brain = require('brain.js');

//
// Configuration
//

let configuration = {
	client: {
	  username: "shayderbot",
		channels: [ 'noiki', 'shayderbot', 'Penacho_' ],
		oAuth: process.env.OAUTH_TOKEN
	},
	targets: ["shayde_revelle", "fatshady07"],
	owner: "penacho_",
	bot: {
		name: "ShayderBot",
		ready: true,
		commands: {
			prefix: "!",
			addName: true
		},
		modules: {
			shots: {
				enabled: true,
				count: 0
			},
			aan: {
				enabled: true,
				joinedChannels: true
			}
		}
	}
};


//
// Initialize Components Function
//

function initialize() {

  // Initalize SqLite Database
	let database =  new sqlite3.Database('shayde.db', (err) => {
	 if(err) console.err(err);
	});

	// Create Required Database Tables
	database.run('CREATE TABLE IF NOT EXISTS messages (id INTEGER PRIMARY KEY AUTOINCREMENT, message TEXT UNIQUE ON CONFLICT IGNORE, channel TEXT, origin TEXT)');
	database.run('CREATE TABLE IF NOT EXISTS masters (id INTEGER PRIMARY KEY AUTOINCREMENT, master TEXT UNIQUE ON CONFLICT IGNORE)');

  // Base Commands for the Bot
	var commands = {
    help: configuration.bot.commands.prefix + "help",
		off: configuration.bot.commands.prefix + "off",
		on: configuration.bot.commands.prefix + "on",
		masteradd: configuration.bot.commands.prefix + "masteradd",
		masters: configuration.bot.commands.prefix + "masters",
		masterrm: configuration.bot.commands.prefix + "masterrm",
		aanon: configuration.bot.commands.prefix + "aanon",
		aanoff: configuration.bot.commands.prefix + "aanoff",
		addmsg: configuration.bot.commands.prefix + "addmsg",
		shotset: configuration.bot.commands.prefix + "shotset",
		shotup: configuration.bot.commands.prefix + "shotup",
		shots: configuration.bot.commands.prefix + "shots",
		shotdown: configuration.bot.commands.prefix + "shotdown",
		shotreset: configuration.bot.commands.prefix + "shotreset"
	};

	// Add BotName as Prefix to Commands
	if (configuration.bot.commands.addName) {
		Object.keys(commands).forEach((cmd) => {
      var command = cmd.split(configuration.bot.commands.prefix);
      commands[cmd] = configuration.bot.commands.prefix + configuration.bot.name + command;
		});
	}

  // Twitch Client
	let client = new tmi.Client({
		options: { debug: true },
		connection: {
			reconnect: true,
			secure: true
		},
		identity: {
			username: configuration.client.username,
			password: 'oauth:' + configuration.client.oAuth
		},
		channels: configuration.client.channels
	});

	// ANN Declaration
	let network = new brain.NeuralNetwork({ hiddenLayers: [4] });

  // Return Values
	return [database, commands, client, network];
}

// Get Required Values from Initialize
const [database, commands, client, network] = initialize();


// Start Twitch Client
client.connect(); // Connect Twitch Client


//
// Shayde History
//
client.on('message', (channel, tags, message, self) => {
	if(self) return;

	//
  // Maintain Functions
  //

  // Show Help (Git Reference)
  if (message.startsWith(commands.help)) {
	  client.say(channel, "Name: " + configuration.botName + ", README: https://gitlab.com/oliverbaehler/shay-tts-bot/blob/master/README.md" );

  // Disable ShaydeBot Functions
  } else if (message.startsWith(commands.off)) {
		if (helpers.isMaster(`${tags.username}`)) {
	    configuration.bot.ready = false;
	    console.log(configuration.botName + " is currently offline");
		}

  // Enable ShydeBot Functions
  } else if (message.startsWith(commands.on)) {
		if (helpers.isMaster(`${tags.username}`)) {
	    configuration.bot.ready = true;
	    client.say(channel, configuration.botName + " ready to serve");
	  }
  }


	//
	// Shayder Functions
	//
	if(configuration.bot.ready) {

		//
		// Master Actions
		//

		// Add a Master
		if (message.startsWith(commands.masteradd)) {
			if (helpers.isMaster(`${tags.username}`)) {

        let [ userReferenced, userUnreferenced ] = helpers.userParameter(message, '!masteradd '); // Get User

        // Insert into Database (Ignore on duplicate)
				database.all('INSERT OR IGNORE INTO masters VALUES (NULL, ?)', [userUnreferenced], function(err, result) {
					  if (err) {
					  	console.log(err); // Log Error
				   	} else {
					  	client.say(channel, "Hello Master, " + userReferenced + " Keepo"); // Reply to Channel
				  	}
				 });

			} else {
				client.say(channel, helpers.unpermittedReply(`${tags.username}`));
			}

		// Show Masters
	} else if (message.startsWith(commands.masters)) {

			// Fetch all Masters
			database.all('SELECT master FROM masters', function(err, result) {
					if (err) {
						console.log(err);
					} else {
            var mastReply; // Boxer

						// Message Output depending on Query
						if (result != undefined && result.length > 0) {

							var names = "";
							// Add Each Name nice stille
              for(i = 0; i <= result.length - 1;  i++) {
                 names += "@" + result[i]['master']; // Add Name element
								 if (i != result.length - 1) names += ", " // Append comma, if not last element
							}
							mastReply = "I obey - " + names;

						} else {
							mastReply = "I don't have any masters yet BibleThump"
						}

						client.say(channel, mastReply + ", Owner: " + "@" + configuration.owner); // Reply to Channel
					}
				});

		// Remove a Master (Owner Only)
  	}	else if (message.startsWith(commands.masterrm)) {

			// Check if Caller is Owner
      if(`${tags.username}` == configuration.owner) {

        let [ userReferenced, userUnreferenced ] = helpers.userParameter(message, '!masterrm '); // Get User

				// Remove Master from Database
				database.run('DELETE FROM masters WHERE master= "' + userUnreferenced + '"', function(err, result) {
						if (err) {
							console.log(err);
						} else {
							client.say(channel, "Hurts, " + userReferenced + " Keepo"); // Reply to Channel
						}
				});

			} else {
				client.say(channel, helpers.unpermittedReply(`${tags.username}`));
			}


	//
	// ANN Training/Messages
	//

	// Enable ANN training mode
  } else if (message.startsWith(commands.aanon)) {
		if (helpers.isMaster(`${tags.username}`)) {
		  aanState = true;
		  console.log("Enabled AAN training");
		} else {
			client.say(channel, helpers.unpermittedReply(`${tags.username}`));
		}

	// Disable ANN training mode
  } else if (message.startsWith(commands.aanoff)) {
		if (helpers.isMaster(`${tags.username}`)) {
			aanState = false;
			console.log("Disabled AAN training");
		} else {
			client.say(channel, helpers.unpermittedReply(`${tags.username}`));
		}

  // Add custom Target Message
  } else if (message.startsWith(commands.addmsg)) {
		if (helpers.isMaster(`${tags.username}`)) {
			helpers.addMessage(message.replace(commands.addmsg + ' ',''), channel, `${tags.username}`, database);
		} else {
			client.say(channel, helpers.unpermittedReply(`${tags.username}`));
		}


	//
	// Shot Commands
	//
  } else if(configuration.bot.modules.shots.enabled && message.startsWith('shot')) {

    // Define Specific amount of shots
		if(message.startsWith(commands.shotset)) {
			if (helpers.isMaster(`${tags.username}`)) {
				try {
					var number = parseInt(message.replace(commands.shotset + ' ',''));
					shotcount = number;
				} catch(err) {
					console.log(err);
				}
			} else {
				client.say(channel, helpers.unpermittedReply(`${tags.username}`));
			}


		// Increment Shot Counter by One
  	} else if(message.startsWith(commands.shotup)) {
			if (helpers.isMaster(`${tags.username}`)) {
				configuration.bot.modules.shots.count = configuration.bot.modules.shots.count + 1;
				client.say(channel, helpers.shotReply(configuration.bot.modules.shots.count)); // Response
			} else {
				client.say(channel, helpers.unpermittedReply(`${tags.username}`)); // Unpermitted Response
			}

	  // Decrement Shot Counter by One
	  } else if(message.startsWith(commands.shotdown)) {
			if (isMaster(`${tags.username}`)) {
				if (configuration.bot.modules.shots.count != 0) {
					configuration.bot.modules.shots.count = configuration.bot.modules.shots.count - 1;
				}

			  client.say(channel, helpers.shotReply(configuration.bot.modules.shots.count)); // Response
			} else {
				client.say(channel, helpers.unpermittedReply(`${tags.username}`));
		  }


		// Show current Shotcount
	  } else if(message.startsWith(commands.shots)) {
				client.say(channel, helpers.shotReply(configuration.bot.modules.shots.count)); // Response


		// Reset Shotcount to 0
		} else if(message.startsWith(commands.shotreset)) {
			// Client return Response
			if (helpers.isMaster(`${tags.username}`)) {
				configuration.bot.modules.shots.count = 0;
			} else {
				client.say(channel, helpers.unpermittedReply(`${tags.username}`));
			}

    // Handle Message
		} else {
			console.log("Unknown Shot Command in '" + message + "'");
		}

  //
	// Containing Listeners
	//

	} else {

		// Talk To ShaydeBot
		if (message.includes('@' + configuration.client.username)) {

			// Base Query
			let countQuery = "SELECT id FROM messages";

      // Change Query based on the configuration
			if (!configuration.bot.modules.aan.joinedChannels) {
				countQuery += ' WHERE channel = "' + channel + '"';
			}

			console.log(countQuery);

	    // Get a Random Message
			database.all(countQuery, function(err, countResult) {
				  if (err) {
						console.log(err);
					}

          console.log(countResult);

					// If Result is Empty
					if (countResult != undefined) {

            // Get amount of all returned Ids (Choose random number on returned object)

            console.log(countResult.length);

						var messageId = countResult[Math.floor(Math.random() * countResult.length) + 1]['id']

            // Log random Id
						console.log("Choose message with id " + messageId)

						// Select Message
						// Get a Random Message + randID
						database.get('SELECT message FROM messages WHERE ID=' + randId, function(err, result) {
							  if (err) {
									console.log(err);
								}

								if (result != undefined) {
									client.say(channel, helpers.recreateReference(result['message'],`${tags.username}`));
								} else {
									console.log("Empty Result for Message " + randID)
								}
						});
					} else {
						console.log("Empty Database");
          }
			 });
		}
  }

	} else {
		console.log(configuration.bot.name + " offline");
	}


  //
	// Listen for Message, even when offline
	//

	// Check for each configured Target
	for (var i = 0; i <= configuration.targets.length - 1; i++) {
    // Check if Message was from
		if (`${tags.username}` == configuration.targets[i]) {
			helpers.addMessage(message.replace(commands.addmsg + ' ',''), channel, `${tags.username}`, database);
		}
	}

});
